package br.com.modalgr.foodmodalgr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.modalgr.foodmodalgr.domain.Agenda;

public interface AgendaRepository extends JpaRepository<Agenda, Long> {

}
