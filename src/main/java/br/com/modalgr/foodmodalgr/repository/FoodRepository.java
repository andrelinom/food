package br.com.modalgr.foodmodalgr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.modalgr.foodmodalgr.domain.Food;

public interface FoodRepository extends JpaRepository<Food, Long> {
	
}
