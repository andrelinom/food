package br.com.modalgr.foodmodalgr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.modalgr.foodmodalgr.domain.Doador;

public interface DoadorRepository extends JpaRepository<Doador, Long>{

}
