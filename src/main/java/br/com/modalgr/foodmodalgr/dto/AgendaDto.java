package br.com.modalgr.foodmodalgr.dto;

import java.util.Date;
import java.util.List;

import br.com.modalgr.foodmodalgr.domain.Doador;
import br.com.modalgr.foodmodalgr.domain.Food;

public class AgendaDto {

	private List<Doador> doadores;	
	private List<Food> foods;
	private Long id;
	private Date data;
	private Food food;
	private Doador doador;

	public List<Doador> getDoadores() {
		return doadores;
	}
	public void setDoadores(List<Doador> doadores) {
		this.doadores = doadores;
	}
	
	public List<Food> getFoods() {
		return foods;
	}
	public void setFoods(List<Food> foods) {
		this.foods = foods;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Food getFood() {
		return food;
	}
	public void setFood(Food food) {
		this.food = food;
	}
	public Doador getDoador() {
		return doador;
	}
	public void setDoador(Doador doador) {
		this.doador = doador;
	}
}
