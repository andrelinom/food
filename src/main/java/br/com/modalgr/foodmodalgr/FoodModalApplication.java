package br.com.modalgr.foodmodalgr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//http://www.thymeleaf.org/doc/articles/layouts.html
@SpringBootApplication
public class FoodModalApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodModalApplication.class, args);
	}

}