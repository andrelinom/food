/*package br.com.modalgr.foodmodalgr.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactory", basePackages = {
		"br.com.modalgr.foodmodalgr.repository" })
public class DataSourceDefaultConfig {
	@Primary
	@Bean(name = "dataSource")
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource dataSource() {
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setUsername(ArquivoConfiguracao.getDatasourceUsername());
		driverManagerDataSource.setPassword(ArquivoConfiguracao.getDatasourcePassword());
		driverManagerDataSource.setUrl(ArquivoConfiguracao.getDatasourceUrl());
		driverManagerDataSource.setDriverClassName(ArquivoConfiguracao.getDatasourceDriverClass());
		return driverManagerDataSource;
	}

	@Primary
	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		String[] strings = {"br.com.modalgr.foodmodalgr.domain"};		
		em.setPackagesToScan(strings);
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(aditionalPropeties());

		return em;
	}

	@Bean
	public Properties aditionalPropeties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", ArquivoConfiguracao.getHibernateDialect());
		properties.setProperty("hibernate.show_sql", ArquivoConfiguracao.getJpaShowSql());
		properties.setProperty("hibernate.hbm2ddl.auto", "update");
		return properties;
	}

	@Primary
	@Bean(name = "transactionManager")
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}
}
*/