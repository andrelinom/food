package br.com.modalgr.foodmodalgr.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ArquivoConfiguracao {

	private static Properties prop;
	private static final Logger LOGGER = LoggerFactory.getLogger(ArquivoConfiguracao.class);
	public static final String MESSAGE_FALHA_SISTEMA = "Falha interna de comunicação, tente novamente!";
	
	private ArquivoConfiguracao() {
		throw new IllegalStateException("ArquivoConfiguracao class");
	}
		
	public static String getJpaShowSql() {
		return getProp().getProperty("spring.jpa.show-sql");
	}
	
	public static String getHibernateDialect() {
		return getProp().getProperty("spring.hibernate.dialect");
	}

	public static String getDatasourceUsername() {
		return getProp().getProperty("spring.datasource.username");
	}

	public static String getDatasourcePassword() {
		return getProp().getProperty("spring.datasource.password");
	}

	public static String getDatasourceUrl() {
		return getProp().getProperty("spring.datasource.url");
	}

	public static String getDatasourceDriverClass() {
		return getProp().getProperty("spring.datasource.driver-class-name");
	}
	
		
	public static String getPathAplicacao() {
		return System.getProperty("user.dir")+getBarra();
	}
	
	public static Properties getProp() {
		if (ArquivoConfiguracao.prop == null || ArquivoConfiguracao.prop.isEmpty()) {
			ArquivoConfiguracao.prop = new Properties();
			try {				
				ArquivoConfiguracao.prop
						.load(new FileInputStream(System.getProperty("user.dir")+getBarra()+"conf-food-modal.properties"));
			} catch (IOException e) {				
				LOGGER.error("Erro ao abrir arquivo de configuração --> ", e);				
			}
		}
		return ArquivoConfiguracao.prop;
	}
	
	public static String getBarra() {		
		if ("WIN".equals(System.getProperty("os.name").toUpperCase().substring(0, 3))) {
			return "\\";
		}
		return "/";
	}
}

