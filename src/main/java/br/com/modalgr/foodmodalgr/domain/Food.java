package br.com.modalgr.foodmodalgr.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@SequenceGenerator(name = "FOOD_SEQ", sequenceName = "FOOD_SEQ", initialValue = 1, allocationSize = 1)
public class Food {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FOOD_SEQ")
	private Long id;
	@NotNull
	@Length(min=2, message="O tamanho do Nome do Prato deve ser preenchido no minimo com {min} caracteres")
	@Column(name="nomeprato")
	private String nomePrato;
	
	private BigDecimal valor;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNomePrato() {
		return nomePrato;
	}
	public void setNomePrato(String nomePrato) {
		this.nomePrato = nomePrato;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	@Override
	public String toString() {
		return "Food [id=" + id + ", nomePrato=" + nomePrato + ", valor=" + valor + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nomePrato == null) ? 0 : nomePrato.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Food other = (Food) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nomePrato == null) {
			if (other.nomePrato != null)
				return false;
		} else if (!nomePrato.equals(other.nomePrato))
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}
	
	
	
	
}
