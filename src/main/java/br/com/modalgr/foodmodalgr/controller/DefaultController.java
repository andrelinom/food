package br.com.modalgr.foodmodalgr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.modalgr.foodmodalgr.domain.Agenda;
import br.com.modalgr.foodmodalgr.repository.AgendaRepository;

@Controller
public class DefaultController {
	
	private final AgendaRepository agendaRepository;	

	public DefaultController(AgendaRepository agendaRepository) {
		this.agendaRepository = agendaRepository;
	}
	
    @GetMapping("/")
    public ModelAndView home1() {
    	Iterable<Agenda> agendados = this.agendaRepository.findAll();
		return new ModelAndView("home","agendados",agendados);
    }
    
    @GetMapping("/home")
    public ModelAndView home() {    	
		Iterable<Agenda> agendados = this.agendaRepository.findAll();
		return new ModelAndView("home","agendados",agendados);
    }
  
   /* @RequestMapping(value="/home", params="form",  method=RequestMethod.POST)
    public ModelAndView home(@Valid HomeDTO homeDto, BindingResult result,RedirectAttributes redirect) {
    	Iterable<Health> listaStatus = statusApiService.listaStatus(homeDto.getAmbiente(), homeDto.getTerminal()); 
    	if (listaStatus == null) {		
    		listaStatus = new LinkedList<Health>();
    	}
    
    	homeDto.setListaStatus(listaStatus);    	
    	
		return new ModelAndView("home","homeDto",homeDto);
    }*/

    @GetMapping("/admin")
    public String admin() {
        return "admin";
    }

    @GetMapping("/user")
    public String user() {
    	return "user";
    }

    @GetMapping("/about")
    public String about() {
        return "home";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/403")
    public String error403() {
        return "error/403";
    }

}
