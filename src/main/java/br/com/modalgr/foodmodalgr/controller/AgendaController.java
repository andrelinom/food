package br.com.modalgr.foodmodalgr.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.modalgr.foodmodalgr.domain.Agenda;
import br.com.modalgr.foodmodalgr.repository.AgendaRepository;
import br.com.modalgr.foodmodalgr.repository.DoadorRepository;
import br.com.modalgr.foodmodalgr.repository.FoodRepository;

@Controller
@RequestMapping("/agenda")
public class AgendaController {
	private static final String AGENDA2 = "agenda";
	private final AgendaRepository agendaRepository;
	@Autowired
	private DoadorRepository doadorRepository;
	@Autowired
	private FoodRepository foodRepository;

	
	private static final String AGENDA_URI = "agenda/";
	
	public AgendaController(AgendaRepository agendaRepository) {
		this.agendaRepository = agendaRepository;
	}

	@GetMapping("/")
	public ModelAndView list() {
		Iterable<Agenda> agendados = this.agendaRepository.findAll();
		return new ModelAndView(AGENDA_URI + "list","agendados",agendados);
	}

	@GetMapping("{id}")
	public ModelAndView view(@PathVariable("id") Agenda agenda) {
		return new ModelAndView(AGENDA_URI + "view",AGENDA2,agenda);
	}

	@GetMapping("/novo")
	public ModelAndView createForm() {
		return new ModelAndView(AGENDA_URI + "form",getMapAgenda(new Agenda()));
	}

	@PostMapping(params = "form")
	public ModelAndView create(@Valid Agenda agenda ,BindingResult result,RedirectAttributes redirect) {
		if (result.hasErrors()) {  
				
				Map<String,Object> model = new HashMap<>();
				model.put("doadores",doadorRepository.findAll());
				model.put("foods",foodRepository.findAll());
				model.put(AGENDA2, agenda);
				model.put("formErrors", result.getAllErrors());
					
				
				return new ModelAndView("redirect:/" +AGENDA_URI + "form", model); }
		
		agenda = this.agendaRepository.save(agenda);
		redirect.addFlashAttribute("globalMessage","Agenda gravado com sucesso");
		return new ModelAndView("redirect:/" + AGENDA_URI + "{agenda.id}","agenda.id",agenda.getId());
	}

	@GetMapping(value = "remover/{id}")
	public ModelAndView remover(@PathVariable("id") Long id,RedirectAttributes redirect) {
		this.agendaRepository.delete(id);
		Iterable<Agenda> agendados = this.agendaRepository.findAll();		
		ModelAndView mv = new ModelAndView(AGENDA_URI + "list","agendados",agendados);
		mv.addObject("globalMessage","Agenda removida com sucesso");
	
		return mv;
	}

	@GetMapping(value = "alterar/{id}")
	public ModelAndView createForm(@PathVariable("id") Agenda agenda) {
		return new ModelAndView(AGENDA_URI + "form",getMapAgenda(agenda));		
	}

	private Map<String, Object> getMapAgenda(Agenda agenda) {
		Map<String,Object> model = new HashMap<>();
		model.put("doadores",doadorRepository.findAll());
		model.put("foods",foodRepository.findAll());
		model.put(AGENDA2, agenda);
		return model;
	}

}
