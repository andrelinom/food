package br.com.modalgr.foodmodalgr.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.modalgr.foodmodalgr.domain.Doador;
import br.com.modalgr.foodmodalgr.repository.DoadorRepository;

@Controller
@RequestMapping("/doador")
public class DoadorController {
	private static final String LISTA_DOADORES = "doadores";
	private DoadorRepository doadorRepository;
	private static final String DOADOR_URI = "doador/";

	public DoadorController(DoadorRepository doadorRepository) {
		this.doadorRepository = doadorRepository;
	}

	@GetMapping("/")
	public ModelAndView list() {
		Iterable<Doador> doadores = this.doadorRepository.findAll();
		return new ModelAndView(DOADOR_URI + "list",LISTA_DOADORES,doadores);
	}

	@GetMapping("{id}")
	public ModelAndView view(@PathVariable("id") Doador doador) {
		return new ModelAndView(DOADOR_URI + "view","doador",doador);
	}

	@GetMapping("/novo")
	public String createForm(@ModelAttribute Doador doador) {
		return DOADOR_URI + "form";
	}

	@PostMapping(params = "form")
	public ModelAndView create(@Valid Doador doador,BindingResult result,RedirectAttributes redirect) {
		
		if (result.hasErrors()) { 
			return new ModelAndView(DOADOR_URI + "form","formErrors",result.getAllErrors()); 
			}
		
		doador = this.doadorRepository.save(doador);
		redirect.addFlashAttribute("globalMessage","Doador gravado com sucesso");
		return new ModelAndView("redirect:/" + DOADOR_URI + "{doador.id}","doador.id",doador.getId());
	}

	@GetMapping(value = "remover/{id}")
	public ModelAndView remover(@PathVariable("id") Long id,RedirectAttributes redirect) {
		try {
			this.doadorRepository.delete(id);
		} catch (Exception e) {
			redirect.addFlashAttribute("globalMessage1","Doador não pode ser removido existem relacionamentos");
			return new ModelAndView("redirect:/" + DOADOR_URI + "{doador.id}","doador.id",id);			
		}
		
		Iterable<Doador> doadores = this.doadorRepository.findAll();
		Map<String,Object> model = new HashMap<>();
		model.put(LISTA_DOADORES,doadorRepository.findAll());
		
		ModelAndView mv = new ModelAndView(DOADOR_URI + "list",LISTA_DOADORES,doadores);
		mv.addObject("globalMessage","Doador removido com sucesso");
	
		return mv;
	}

	@GetMapping(value = "alterar/{id}")
	public ModelAndView alterarForm(@PathVariable("id") Doador doador) {
		return new ModelAndView(DOADOR_URI + "form","doador",doador);
	}
}
