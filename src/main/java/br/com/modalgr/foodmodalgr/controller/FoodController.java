package br.com.modalgr.foodmodalgr.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.modalgr.foodmodalgr.domain.Food;
import br.com.modalgr.foodmodalgr.repository.FoodRepository;

@Controller
@RequestMapping("/food")
public class FoodController {
	private FoodRepository foodRepository;
	private static final String FOOD_URI = "food/";

	public FoodController(FoodRepository foodRepository) {
		this.foodRepository = foodRepository;
	}

	@GetMapping("/")
	public ModelAndView list() {
		Iterable<Food> foods = this.foodRepository.findAll();
		return new ModelAndView(FOOD_URI + "list","foods",foods);
	}

	@GetMapping("{id}")
	public ModelAndView view(@PathVariable("id") Food food) {
		return new ModelAndView(FOOD_URI + "view","food",food);
	}

	@GetMapping("/novo")
	public String createForm(@ModelAttribute Food food) {
		return FOOD_URI + "form";
	}

	@PostMapping(params = "form")
	public ModelAndView create(@Valid Food food,BindingResult result,RedirectAttributes redirect) {
		if (result.hasErrors()) { 
			return new ModelAndView(FOOD_URI + "form","formErrors",result.getAllErrors()); 
		}
		food = this.foodRepository.save(food);
		redirect.addFlashAttribute("globalMessage","Food gravado com sucesso");
		return new ModelAndView("redirect:/" + FOOD_URI + "{food.id}","food.id",food.getId());
	}

	@GetMapping(value = "remover/{id}")
	public ModelAndView remover(@PathVariable("id") Long id,RedirectAttributes redirect) {
		try {
			this.foodRepository.delete(id);
			
		} catch (Exception e) {
			redirect.addFlashAttribute("globalMessage1","Doador não pode ser removido existem relacionamentos");
			return new ModelAndView("redirect:/" + FOOD_URI + "{doador.id}","doador.id",id);			
		}
		
		
		Iterable<Food> foods = this.foodRepository.findAll();		
		ModelAndView mv = new ModelAndView(FOOD_URI + "list","foods",foods);
		mv.addObject("globalMessage","Food removido com sucesso");
	
		
		
		
		return mv;
	}

	@GetMapping(value = "alterar/{id}")
	public ModelAndView alterarForm(@PathVariable("id") Food food) {
		return new ModelAndView(FOOD_URI + "form","food",food);
	}

}
